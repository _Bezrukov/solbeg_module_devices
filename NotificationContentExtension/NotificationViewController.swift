//
//  NotificationViewController.swift
//  secondNotificationContentExtension
//
//  Created by Ilya Bezrukov on 7/23/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var osLabel: UILabel!
    @IBOutlet weak var osVerLabel: UILabel!
    @IBOutlet weak var teamLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        let bufDict = notification.request.content.userInfo
        self.nameLabel.text = "Name: \(bufDict["Name"] as? String ?? "")"
        self.typeLabel.text = "Type: \(bufDict["Type"] as? String ?? "")"
        self.modelLabel.text = "Model: \(bufDict["Model"] as? String ?? "")"
        self.osLabel.text = "OS: \(bufDict["OS"] as? String ?? "")"
        self.osVerLabel.text = "OSVer: \(bufDict["OSVersion"] as? String ?? "")"
        self.teamLabel.text = "Team: \(bufDict["Owner"] as? String ?? "")"
        
    }

}
