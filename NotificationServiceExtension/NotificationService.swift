//
//  NotificationService.swift
//  NotificationServiceExtension
//
//  Created by Ilya Bezrukov on 7/24/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import UserNotifications
import MobileCoreServices

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // "attachment-url" - inside of push link field
            if let attachmentString = bestAttemptContent.userInfo["attachment-url"] as? String,
                let attachmentURL = URL(string: attachmentString) {
                let session = URLSession(configuration: URLSessionConfiguration.default)
                let downloadTask = session.downloadTask(with: attachmentURL) { (url, _, error) in
                    if let error = error {
                        debugPrint("Error: \(error.localizedDescription)")
                    } else if let url = url {
                        let attachment = try! UNNotificationAttachment(identifier: attachmentString, url: url, options: [UNNotificationAttachmentOptionsTypeHintKey : kUTTypeMPEG4])
                        bestAttemptContent.attachments = [attachment]
                    }
                    contentHandler(bestAttemptContent)
                }
                downloadTask.resume()
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}


//{
//    "aps": {
//        "alert": {
//            "title": "notification alert",
//            "subtitle": "notification subtitle",
//            "body": "notification body"
//        },
//        "mutable-content": 1
//    },
//    "attachment-url": "https://s3-us-west-2.amazonaws.com/notificationvideos/recipe2.mp4"
//}

// 30 sekund
// ska4ivaiet faily 5 metrov audio, 10 kartinki i gifki, 50 metrov video
// end to end encryption. can decrypt inside of notification. completely secure messages
