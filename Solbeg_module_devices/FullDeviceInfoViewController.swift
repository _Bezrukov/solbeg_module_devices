//
//  FullDeviceInfoViewController.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/2/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import Foundation
import UIKit

class FullDeviceInfoViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var devicesTableView: UITableView!
    
    //MARK: - Globals
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    
    //MARK: - Privates and other methods
}
