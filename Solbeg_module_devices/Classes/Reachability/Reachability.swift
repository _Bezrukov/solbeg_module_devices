//
//  Reachability.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/19/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import CoreLocation

open class Reachability {
    class func isLocationServiceEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            default:
                debugPrint("Something wrong with Location services")
                return false
            }
        } else {
            debugPrint("Location services are not enabled")
            return false
        }
    }
}
