//
//  FirestoreManager.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/4/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class FirestoreManager: NSObject {
    static let shared = FirestoreManager()
    
    private let db: Firestore
    
    private override init(){
        db = Firestore.firestore()
    }
    
    func getDeviceObject(completionHandler: @escaping ([Device]?, Error?, Bool) ->()) {
        db.collection("Devices").getDocuments { [weak self] (querySnapshot, error) in
            if let doc = querySnapshot?.documents {
                completionHandler(self?.convertQueryDocumentSnapshotToDevicesArray(dbResponse: doc), error, true)
            }
        }
    }
    
    func addDeviceObjectToDB(device: Device?, completion: @escaping (Error?) -> ()) {
        guard let rawDevice = device else { return }
        let documentNameToSave = "Device+\(rawDevice.name ?? "")"
        db.collection("Devices").document(documentNameToSave).setData(rawDevice.convertToJson()) { (error) in
            if (error != nil) {
                completion(error)
            } else {
            completion(error)
            }
        }
    }
    
    private func convertDocumentSnapshotToDevice(dbResponse: DocumentSnapshot) -> Device {
        let rawDevice: Device? = Device()
        guard let device = rawDevice?.convertToDevice(dbData: dbResponse.data() ?? [:]) else { return Device.init() }
        return device
    }
    
    private func convertQueryDocumentSnapshotToDevicesArray(dbResponse: [QueryDocumentSnapshot]) -> [Device] {
        var arrayOfDevices = [Device]()
        dbResponse.forEach { (element) in
            arrayOfDevices.append(convertDocumentSnapshotToDevice(dbResponse: element))
        }
        return arrayOfDevices
    }
}
