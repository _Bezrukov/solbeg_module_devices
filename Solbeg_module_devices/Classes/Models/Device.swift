import Foundation
import UIKit

enum DeviceField: String {
    case name = "Name",
    type = "Type",
    model = "Model",
    os = "OS",
    osVersion = "OSVersion",
    owner = "Owner"
}

class Device {
    private(set) var name: String?
    private(set) var type: String?
    private(set) var model: String?
    private(set) var operationSystem: String?
    private(set) var osVersion: String?
    private(set) var teamOwnerName: String?
    let deviceID = UUID()
    
    init(name: String? = nil, type: String? = nil, model: String? = nil, operationSystem: String? = nil, osVersion: String? = nil, teamOwnerName: String? = nil) {
        self.name = name
        self.type = type
        self.model = model
        self.operationSystem = operationSystem
        self.osVersion = osVersion
        self.teamOwnerName = teamOwnerName
    }
    
    func updateName(_ name: String){
        self.name = name
    }
    
    func updateType(_ type: String){
        self.type = type
    }
    
    func updateModel(_ model: String){
        self.model = model
    }
    
    func updateOS(_ os: String){
        self.operationSystem = os
    }
    
    func updateOSVersion(_ osVersion: String){
        self.osVersion = osVersion
    }
    
    func updateTeamOwnerName(_ teamOwnerName: String){
        self.teamOwnerName = teamOwnerName
    }
    
}

//MARK: - Firebase converter
extension Device {
    func convertToJson() -> [String: Any] {
        var bufDict: [String: Any] = [:]
        bufDict[DeviceField.name.rawValue] = name
        bufDict[DeviceField.type.rawValue] = type
        bufDict[DeviceField.model.rawValue] = model
        bufDict[DeviceField.os.rawValue] = operationSystem
        bufDict[DeviceField.osVersion.rawValue] = osVersion
        bufDict[DeviceField.owner.rawValue] = teamOwnerName
        return bufDict
    }
    func convertToDevice(dbData: [String: Any]) -> Device {
        let bufDevice = Device(name: dbData[DeviceField.name.rawValue] as? String,
                                   type: dbData[DeviceField.type.rawValue] as? String,
                                   model: dbData[DeviceField.model.rawValue] as? String,
                                   operationSystem: dbData[DeviceField.os.rawValue] as? String,
                                   osVersion: dbData[DeviceField.osVersion.rawValue] as? String,
                                   teamOwnerName: dbData[DeviceField.owner.rawValue] as? String)
        
        return bufDevice
    }
}
