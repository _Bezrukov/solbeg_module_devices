//
//  AddNewDeviceViewController.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/8/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import Foundation
import Firebase
import UIKit

final class AddNewDeviceViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    @IBOutlet weak var osTextField: UITextField!
    @IBOutlet weak var osVersionTextField: UITextField!
    @IBOutlet weak var teamOwnerTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    //MARK: - Public
    let loadingView = LoadingView()
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - IBActions
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        onSaveAction()
    }
    
    //MARK: - Privates and other methods
    private func onSaveAction(){
        loadingViewState(is: true)
        let bufDeviceObject = Device(name: nameTextField.text,
                                     type: typeTextField.text,
                                     model: modelTextField.text,
                                     operationSystem: osTextField.text,
                                     osVersion: osVersionTextField.text,
                                     teamOwnerName: teamOwnerTextField.text)
        FirestoreManager.shared.addDeviceObjectToDB(device: bufDeviceObject) { [weak self] (error) in
            if (error != nil) {
                self?.loadingViewState(is: false)
                self?.showErrorAlert()
            } else {
                self?.loadingViewState(is: false)
                NotificationManager.shared.addNotificationWithNewDeviceInfo(device: bufDeviceObject, completion: { (error) in
                    if (error != nil) {
                        self?.showErrorAlert()
                    }
                })
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    private func loadingViewState(is flag: Bool) {
        if flag {
            self.view.addSubview(loadingView)
        } else {
            loadingView.removeFromSuperview()
        }
    }
}
