//
//  FullDeviceInfoViewController.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/2/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import Foundation
import UIKit

class FullDeviceInfoViewController: UIViewController {
    
    static func prepare(with device: Device) -> FullDeviceInfoViewController {
        let devicesStoryboard = UIStoryboard(name: "Devices", bundle: nil)
        let vc = devicesStoryboard.instantiateViewController(withIdentifier: UIStoryboard.Identifier.fullDeviceInfoViewController.rawValue) as! FullDeviceInfoViewController
        vc.configure(device: device)
        return vc
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var osLabel: UILabel!
    @IBOutlet weak var osVersionLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    
    private var device: Device!
    
    //MARK: - Public
    func configure(device: Device) {
        self.device = device
    }
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "📱"
        // Do any additional setup after loading the view.
        setupLabels()
    }
    
    //MARK: - IBActions
    
    //MARK: - Privates and other methods
    private func setupLabels(){
        nameLabel.text = device.name
        typeLabel.text = device.type
        modelLabel.text = device.model
        osLabel.text = device.operationSystem
        osVersionLabel.text = device.osVersion
        ownerLabel.text = device.teamOwnerName
    }
}
