//
//  ViewController.swift
//  Solbeg_module_devices
//
//  Created by admin on 6/28/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//  

import Foundation
import UIKit
import Firebase
import CoreLocation
import Lottie

final class DevicesViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var devicesTableView: UITableView!
    @IBOutlet weak var gifButton: UIButton!
    
    //MARK: - Globals
    var dataForTableView: [Device] = []
    var listOfDevices: [String] = []
    let loadingView = LoadingView()
    var locationManager: CLLocationManager?
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        loactionPremissionRequestAndDelegateSetup()
        notificationsSetupAndAdding()
        // Do any additional setup after loading the view.
        setupTableView()
        setupButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FullDeviceInfoViewController,
            let path = sender as? IndexPath {
            vc.configure(device: dataForTableView[path.row])
        }
        super.prepare(for: segue, sender: sender)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingViewState(is: true)
        getDevices()
    }
    
    //MARK: - IBActions
    @IBAction func gifButtonTapped(_ sender: UIButton) {
        NotificationManager.shared.addNotificationWithGifAttachment { (error) in
            if (error != nil) {
                self.showErrorAlert()
            }
        }
    }
    
    //MARK: - Privates and other methods
    private func loadingViewState(is flag: Bool) {
        if flag {
            self.view.addSubview(loadingView)
        } else {
            loadingView.removeFromSuperview()
        }
    }
    
    private func setupTableView() {
        devicesTableView.delegate = self
        devicesTableView.dataSource = self
    }
    
    private func getDevices() {
        FirestoreManager.shared.getDeviceObject() { (arrayOfDeviceObjects, error, flag) in
            if (error != nil) {
                debugPrint(error as? String ?? "")
                self.showErrorAlert()
                self.loadingViewState(is: false)
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.dataForTableView = arrayOfDeviceObjects ?? []
                    if flag == true {
                        self?.devicesTableView.reloadData()
                        self?.loadingViewState(is: false)
                    }
                }
            }
        }
    }
    
    private func loactionPremissionRequestAndDelegateSetup() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self as? CLLocationManagerDelegate
        locationManager?.requestAlwaysAuthorization()
    }
    
    private func setupButton() {
        gifButton.layer.cornerRadius = gifButton.frame.height / 2
        gifButton.backgroundColor = .white
        gifButton.layer.shadowColor = UIColor.black.cgColor
        gifButton.layer.shadowOpacity = 0.3
        gifButton.layer.shadowRadius = 4
        gifButton.layer.shadowOffset = CGSize(width: -5, height: -5)
    }
    
    private func notificationsSetupAndAdding() {
        NotificationManager.shared.addNotificationWithPNGattachment { (error) in
            if (error != nil) {
                self.showErrorAlert()
            }
        }
        
        NotificationManager.shared.addNotificationWithReinstallReminder { (error) in
            if (error != nil) {
                self.showErrorAlert()
            }
        }
        NotificationManager.shared.addNotificationWithCalendarTrigger { (error) in
            if (error != nil) {
                self.showErrorAlert()
            }
        }
        
        if Reachability.isLocationServiceEnabled() {
            NotificationManager.shared.addNotificationWithLeavingLocationTrigger { (error) in
                if (error != nil) {
                    self.showErrorAlert()
                }
            }
            NotificationManager.shared.addNotificationWithEnteringLocationTrigger { (error) in
                if (error != nil) {
                    self.showErrorAlert()
                }
            }
        } else {
            showLocationAllert()
        }
        
    }
}

//MARK: - UITableViewDelegate
extension DevicesViewController: UITableViewDelegate {
    
}

//MARK: - UITableViewDataSource
extension DevicesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataForTableView.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DeviceCell.identifier, for: indexPath) as! DeviceCell
        cell.nameLabel.text = dataForTableView[indexPath.row].name
        cell.typeLabel.text = dataForTableView[indexPath.row].type
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationManager.shared.addNotificationWithTimeIntervalTrigger { (error) in
            if (error != nil) {
                self.showErrorAlert()
            }
        }
        NotificationManager.shared.addNotificationWithDelayedFullInfo { (error) in
            if (error != nil) {
                self.showErrorAlert()
            }
        }
        performSegue(withIdentifier: UIViewController.segueIdentifiers.showFullDeviceInfo.rawValue, sender: indexPath)
    }
    
}
