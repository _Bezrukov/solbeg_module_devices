//
//  UIViewController.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/19/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import UIKit


extension UIViewController {
    
    enum segueIdentifiers: String {
        case showFullDeviceInfo = "showFullDeviceInfo"
    }
    
    func showErrorAlert() {
        let alert = UIAlertController(title: NSLocalizedString("ERROR", comment: ""), message: NSLocalizedString("DEFUALT_ERROR_DESCRIPTION", comment: ""), preferredStyle: .alert)
        let alertCancelAction = UIAlertAction(title: NSLocalizedString("GOT_IT_ACT", comment: ""), style: .cancel, handler: nil)
        alert.addAction(alertCancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showLocationAllert() {
        
        let alert = UIAlertController(title: NSLocalizedString("LOCATION_ERROR", comment: ""), message: NSLocalizedString("LOCATION_ERROR_DESCRIPTION", comment: ""), preferredStyle: UIAlertController.Style.alert)
        
        // Button to Open Settings
        alert.addAction(UIAlertAction(title: NSLocalizedString("SETTINGS_ACT", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    debugPrint("Settings opened: \(success)")
                })
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK_ACT", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        OperationQueue.main.addOperation {
            self.present(alert, animated: true,
                         completion:nil)
        }
    }
}
