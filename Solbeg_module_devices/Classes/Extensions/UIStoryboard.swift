//
//  UIStoryboard.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/17/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import UIKit

extension UIStoryboard {

    enum Identifier: String {
        case devicesViewController = "DevicesViewController"
        case fullDeviceInfoViewController = "FullDeviceInfoViewController"
        case addNewDeviceViewController = "AddNewDeviceViewController"
    }
    
    func instantiateViewControllerWithIdentifier(with identifier: Identifier) -> UIViewController {
        return self.instantiateViewController(withIdentifier: identifier.rawValue)
    }
    
}
