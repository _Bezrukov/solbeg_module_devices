//
//  NotificationManager.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/18/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

enum requestIdentifiers: String {
    case timeInterval = "timeInterval"
    case calendar = "calendar"
    case leavingLocation = "leavingLocation"
    case timeIntervalNewDevice = "timeIntervalNewDevice"
    case reinstallReminder = "reinstallReminder"
    case enteringLocation = "enteringLocation"
    case delayedTimeIntervalFullInfo = "delayedTimeIntervalFullInfo"
    case gifAttachment = "gifAttachment"
    case pngAttachment = "pngAttachment"
}

enum actionsIdentifiers: String {
    case openApp = "openApp"
    case close = "close"
    case callSoyExpress = "callSoyExpress"
}

enum categoriesIdentifiers: String {
    case withOpenAndClose = "categoryWithOpenAndClose"
    case forCustomExtension = "myNotificationCategory"
    case withCallAndClose = "withCallAndClose"
}

enum attachmentIdentifiers: String {
    case gif = "gif"
    case jpg = "jpg"
    case png = "png"
}

class NotificationManager: NSObject {
    
    static let shared = NotificationManager()
    let center = UNUserNotificationCenter.current()
    
    override init() {
        super.init()
       
    }
    func configure() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        center.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                debugPrint(NSLocalizedString("DECLINED_NOTIFICATIONS_PERMISSION", comment: ""))
            }
        }
        UIApplication.shared.registerForRemoteNotifications()
        center.delegate = self
    }
    
    func addNotificationWithPNGattachment(completion: @escaping (Error?) -> ()) {
        createActionsAndCategories()
        let content = createContent(title: NSLocalizedString("PNG_ATTACHMENT_TITLE", comment: ""), body: NSLocalizedString("PNG_ATTACHMENT_BODY", comment: ""))
        content.categoryIdentifier = categoriesIdentifiers.withCallAndClose.rawValue
        
        var pngUrl: URL?
        pngUrl = Bundle.main.url(forResource: "box_new", withExtension: "png")
        
        do {
            let pngAttachment = try UNNotificationAttachment(identifier: attachmentIdentifiers.png.rawValue, url: pngUrl!, options: nil)
            content.attachments = [pngAttachment]
        } catch {
            debugPrint(NSLocalizedString("ERROR", comment: ""))
        }
        
        var dateComponents = DateComponents()
        dateComponents.hour = 15
        dateComponents.minute = 50
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.pngAttachment.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            completion(error)
        }
    }
    
    func addNotificationWithGifAttachment(completion: @escaping (Error?) -> ()) {
        let content = createContent(title: NSLocalizedString("GIF_ATTACHMENT_TITLE", comment: ""), body: NSLocalizedString("GIF_ATTACHMENT_BODY", comment: ""))
        var gifUrl: URL?
        
        gifUrl = Bundle.main.url(forResource: "never", withExtension: "gif")
        do {
            let gifAttachment = try UNNotificationAttachment(identifier: attachmentIdentifiers.gif.rawValue, url: gifUrl!, options: nil)
            
            content.attachments = [gifAttachment]
        } catch {
            debugPrint(NSLocalizedString("ERROR", comment: ""))
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.gifAttachment.rawValue, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            completion(error)
        }
    }
    
    func addNotificationWithDelayedFullInfo(completion: @escaping (Error?) -> ()) {
        let content = createContent(title: NSLocalizedString("DELAYED_TITLE", comment: ""), body: NSLocalizedString("DELAYED_BODY", comment: ""))
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 300, repeats: false)
        
        content.categoryIdentifier = categoriesIdentifiers.forCustomExtension.rawValue
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.delayedTimeIntervalFullInfo.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            completion(error)
        }
        
    }
    
    func addNotificationWithTimeIntervalTrigger(completion: @escaping (Error?) -> ()) {
        createActionsAndCategories()
        let content = createContent(title: NSLocalizedString("TIME_INTERVAL_TITLE", comment: ""), body: NSLocalizedString("TIME_INTERVAL_BODY", comment: ""))
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        content.categoryIdentifier = categoriesIdentifiers.withOpenAndClose.rawValue
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.timeInterval.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
           completion(error)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            UIApplication.shared.applicationIconBadgeNumber += 1
        }
    }
    
    func addNotificationWithCalendarTrigger(completion: @escaping (Error?) -> ()) {
        let content = createContent(title: NSLocalizedString("CALENDAR_TITLE", comment: ""), body: NSLocalizedString("CALENDAR_BODY", comment: ""))
        
        // will send notification every day at 11:15 am
        var components = DateComponents()
        components.hour = 11
        components.minute = 15
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.calendar.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            completion(error)
        }
    }
    
    func addNotificationWithLeavingLocationTrigger(completion: @escaping (Error?) -> ()) {
        let content = createContent(title: NSLocalizedString("LEAVING_LOCATION_TITLE", comment: ""),
                                    body: NSLocalizedString("LEAVING_LOCATION_BODY", comment: ""))
        
        // 53.955580, 27.620013 Belarus, Minsk, Solbeg soft office
        let coordinatesCenter = CLLocationCoordinate2DMake(53.955580, 27.620013)
        let region = CLCircularRegion(center: coordinatesCenter, radius: 50, identifier: UUID().uuidString)
        
        // additional region setup
        region.notifyOnEntry = false
        region.notifyOnExit = true
        
        let trigger = UNLocationNotificationTrigger(region: region, repeats: true)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.leavingLocation.rawValue, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            completion(error)
        }
    }
    
    func addNotificationWithEnteringLocationTrigger(completion: @escaping (Error?) -> ()) {
        let content = createContent(title: NSLocalizedString("ENTERING_TITLE", comment: ""),
                                    body: NSLocalizedString("ENTERING_BODY", comment: ""))
        
        // 53.955580, 27.620013 Belarus, Minsk, Solbeg soft office
        let coordinatesCenter = CLLocationCoordinate2DMake(53.955580, 27.620013)
        let region = CLCircularRegion(center: coordinatesCenter, radius: 50, identifier: UUID().uuidString)
        
        // additional region setup
        region.notifyOnEntry = true
        region.notifyOnExit = false
        
        let trigger = UNLocationNotificationTrigger(region: region, repeats: true)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.leavingLocation.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            completion(error)
        }
    }
    
    func addNotificationWithNewDeviceInfo(device: Device, completion: @escaping (Error?) -> ()) {
        let content = createContent(title: NSLocalizedString("NEW_DEVICE_TITLE", comment: ""), body: NSLocalizedString("NEW_DEVICE_BODY", comment: ""))
        content.categoryIdentifier = categoriesIdentifiers.forCustomExtension.rawValue
        content.userInfo = device.convertToJson()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.timeIntervalNewDevice.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            completion(error)
        }
    }
    
    func addNotificationWithReinstallReminder(completion: @escaping (Error?) -> ()){
        let content = createContent(title: NSLocalizedString("REBUILD_TITLE", comment: ""), body: NSLocalizedString("REBUILD_BODY", comment: ""))
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60*60*24*6.5, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifiers.reinstallReminder.rawValue, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            completion(error)
        }
        
    }

    //MARK: - Privates
    private func createContent(title: String?, body: String?) -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        if let unwrappedTitle = title, let unwrappedBody = body {
            content.title = unwrappedTitle
            content.body = unwrappedBody
        }
        return content
    }
    
    private func createActionsAndCategories() {
        // Actions
        let actionOpenApp = UNNotificationAction(identifier: actionsIdentifiers.openApp.rawValue, title: NSLocalizedString("OPEN_APP_ACT", comment: ""), options: .authenticationRequired)
        let actionClose = UNNotificationAction(identifier: actionsIdentifiers.close.rawValue, title: NSLocalizedString("CLOSE_ACT", comment: ""), options: .destructive)
        let actionCall = UNNotificationAction(identifier: actionsIdentifiers.callSoyExpress.rawValue, title: NSLocalizedString("CALL_SOY_EXPRESS_ACT", comment: ""), options: .foreground)
        
        // Categories
        let categoryWithOpenAndClose = UNNotificationCategory(identifier: categoriesIdentifiers.withOpenAndClose.rawValue, actions: [actionOpenApp, actionClose], intentIdentifiers: [], options: [])
        let categoryWithCallAndClose = UNNotificationCategory(identifier: categoriesIdentifiers.withCallAndClose.rawValue, actions: [actionCall, actionClose], intentIdentifiers: [], options: [])
        
        
        // Adding categories
        UNUserNotificationCenter.current().setNotificationCategories([categoryWithOpenAndClose, categoryWithCallAndClose])
    }
    
    private func callNumber(phoneNumber: String) {
        
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
}

extension NotificationManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.actionIdentifier == actionsIdentifiers.callSoyExpress.rawValue {
            callNumber(phoneNumber: "+375291902222")
        }
        completionHandler()
    }
}
