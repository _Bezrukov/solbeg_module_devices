//
//  LoadingView.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/12/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import UIKit
import Lottie

class LoadingView: LottieView {
    
    let lottieAnimationView = AnimationView(name: "353-newspaper-spinner")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        guard let superviewFrame = newSuperview?.frame else { return }
        self.frame = superviewFrame
        self.backgroundColor = .lightGray
        self.alpha = 0.7
//        let activityIndicator = UIActivityIndicatorView()
//        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//        activityIndicator.center = self.center
//        activityIndicator.style = .whiteLarge
//        self.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
        lottieAnimationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        lottieAnimationView.center = self.center
        lottieAnimationView.contentMode = .scaleAspectFill
        lottieAnimationView.loopMode = .loop
        self.addSubview(lottieAnimationView)
        lottieAnimationView.play()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        lottieAnimationView.stop()
    }
}
