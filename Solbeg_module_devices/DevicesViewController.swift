//
//  ViewController.swift
//  Solbeg_module_devices
//
//  Created by admin on 6/28/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//  

import UIKit

class DevicesViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var devicesTableView: UITableView!
    
    //MARK: - Globals
    
    var fakeDataForNow: [BaseDevice] = []

    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    //MARK: - IBActions

    //MARK: - Privates and other methods
}

//MARK: - UITableViewDelegate
extension DevicesViewController: UITableViewDelegate {
    
}

//MARK: - UITableViewDataSource
extension DevicesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fakeDataForNow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DeviceCell.identifier, for: indexPath)
        return cell
    }
    
    
}

