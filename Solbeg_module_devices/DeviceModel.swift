//
//  BaseDeviceModel.swift
//  Solbeg_module_devices
//
//  Created by Ilya Bezrukov on 7/2/19.
//  Copyright © 2019 Ilya Bezrukov. All rights reserved.
//

import Foundation

final class DeviceModel {
    private(set) var fakeDevices : [BaseDevice] = []
    
    init() {
        fakeDevices.append(BaseDevice(name: "1", type: "2", model: "3", operationSystem: "4", osVersion: "5", teamOwnerName: "6"))
        fakeDevices.append(BaseDevice(name: "11", type: "22", model: "33", operationSystem: "44", osVersion: "55", teamOwnerName: "66"))
    }
    
    func addNewItem() {
        
    }
}
