import Foundation
import UIKit

class BaseDevice {
    let name: String
    let type: String
    let model: String
    let operationSystem: String
    let osVersion: String
    let teamOwnerName: String
    let DeviceID = UUID()
    
    init(name: String, type: String, model: String, operationSystem: String, osVersion: String, teamOwnerName: String) {
        self.name = name
        self.type = type
        self.model = model
        self.operationSystem = operationSystem
        self.osVersion = osVersion
        self.teamOwnerName = teamOwnerName
    }
}
